#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
2. Se tiene un \matriz" de 15 filas y 12 columnas. Realice un programa en Python que permita leer la
matriz, calcule y presente los resultados siguientes:

-El menor elemento de la matriz
-La suma de los elementos de las cinco primeras columnas de cada fila de la matriz.
-Muestre el total de elementos negativos en las columnas de la quinta a la nueve.
"""
import random

#función para encontrar los elementos negativos
def elementos_negativos(matriz):
    negativos = []
    total = 0
    #busca entre todas las filas, pero en un area definida de columnas
    for i in range(15):
        for j in range(5, 9):
            #elementos negativos son menores a 0
            if matriz[i][j] < 0:
                negativos.append(matriz[i][j])
    #función para sumar esos negativos
    for i in range(15):
        for j in range(5, 9):
            if matriz[i][j] < 0:
                total += matriz[i][j]
    print("\n Los numeros negativos desde la columna 5 a la 9 son:", negativos)
    print(" Y su suma total es de:", total)

#función para sumar elementos de columnas
def suma_elementos(matriz):
    suma = []
    #busca en matriz generando una lista con las sumas de las columnas
    for i in range(5):
        temp = 0
        for j in range(15):
            temp += matriz[j][i]
        suma.append(temp)
    print("\n La suma de los elementos de cada una de las primeras 5 columnas son:")
    num = 0
    #impresión de manera ordenada
    for i in suma:
        num += 1 
        print("\n Columna",num, ":", i)

#función para buscar el menor elemento de la matriz
def elemento_menor(matriz):
    menor = matriz[0][0]
    #recorre la matriz en busca del menor numero
    for i in range(15):
        for j in range(12):
            #cada vez que se encuentra el menor se guarda para volver a compararse
            if menor > matriz[i][j]:
                menor = matriz[i][j]
    print("\n El menor numero encontrado en la matriz es:", menor)

#función que crea matriz
def crea_matriz(matriz):
        # crea lista con sublistas
    for i in range(15):
        # de manera vacía
        matriz.append([])
        for j in range(12):
            # a cada i se le da un valor aleatorio entre negativos y positivos
            matriz[i].append(random.randint(-10, 10))
        # imprime matriz
        print(" ", matriz[i], " ")

#función main
if __name__ == '__main__':
    matriz = []
    crea_matriz(matriz)
    elemento_menor(matriz)
    suma_elementos(matriz)
    elementos_negativos(matriz)
