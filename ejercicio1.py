#!/usr/bin/env python3
# -*- coding:utf-8 *-*
"""
1. Realice un programa que lea una tupla que usted defina de n elementos. A partir de esa tupla rellene
una lista (por defecto debe estar vacia) de tal forma que el primer elemento pase a ser el segundo, el
segundo pase a ser el tercero, el ultimo pase a ser el primero, y asi sucesivamente hasta recorrer toda
la tupla.
"""
#función cambia tupla a lista
def crea_lista(tupla, lista):
    #el primer valor de la lista será el último de la tupla
    #y el resto seguirá el orden de la tupla con su mismo tamaño
    lista = [tupla[len(tupla)-1] if i == 0 else tupla[i-1] for i in range(len(tupla))]
    print (lista)

#función main
if __name__ == "__main__":
    tupla = (1, 2, 3, 4, 5, 6)
    lista = []
    crea_lista(tupla, lista)
