#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Se pide calcular los notas de los 31 estudiantes de programacion 1 e indicar quien aprueba (>= 4) o
quien reprueba (< 4).
Para ello se debe obtener las notas (que las puede generar de modo aleatorio entre 1 y 7 en tipo oat)
pero tambien hay que reconocer al estudiante.
"""
import random

#función que agrega los estudiantes a las listas de promedios
def alumnos(estudiantes):
    alumnos = estudiantes
    n = 1
    #se voltea cada lista y se le agrega un número que va sumando
    for i in alumnos:
        i.reverse()
        i.append(n)
        i.reverse()
        n += 1
        print(i)

#función calcula el promedio de cada estudiante
def total_promedio(estudiantes):
    total_aprueba = 0
    total_reprueba = 0
    alumno = 1
    #ciclo que evalua cada estudiante
    for i in estudiantes:
        promedio = 0
        suma = sum(i)
        promedio = round(suma/ 5, 1)
        print("\n El estudiante", alumno, "promedia con nota:", promedio)
        if promedio >= 4:
            print("  >Aprueba el curso")
            total_aprueba += 1
        else:
            print("  >Reprueba el curso")
            total_reprueba += 1
        alumno += 1
    print("la cantidad de estudiantes que aprobaron son: ", total_aprueba)
    print("la cantidad de estudiantes que reprobaron son: ", total_reprueba)

def crea_matriz(estudiantes):
    num = 1
        # crea lista con sublistas
    for i in range(31):
        # de manera vacía
        estudiantes.append([])
        for j in range(5):
            # a cada i se le da un valor aleatorio entre negativos y positivos
            estudiantes[i].append (round(random.uniform(1.0,7.0),1))
        # imprime matriz
        # ~ print(" ", estudiantes[i], " ")


if __name__ == '__main__':
    
    estudiantes = []
    crea_matriz(estudiantes)
    alumnos(estudiantes)
    total_promedio(estudiantes)
